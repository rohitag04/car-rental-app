
jQuery(document).ready(function() {
    /*Hide Result Divs*/
    $("#result").hide();
    $("#carResult").hide();
    
    /*Validation Action Performed*/
    $('#btn').click(function (e) {
            var isValid = true;
            $('#form-pick-up,#pickUpDate,#startTime,#dropOffDate,#endTime').each(function () {
                if ($.trim($(this).val()) === '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                    
                }
            });
            if (isValid === false)
                e.preventDefault();
            clickAction();

        });

    
    /* Datepicker Jquery*/
    $("#pickUpDate").datepicker({
        minDate: 0
    });
    
    $("#dropOffDate").datepicker({
        minDate: 0
    });
    
    
    /* button action performed */
function clickAction(){
            var pickUp = $("#form-pick-up").val();
        var startDate = $("#pickUpDate").val();
        var startTime = $("#startTime").val();
        var endDate = $("#dropOffDate").val();
        var endTime = $("#endTime").val();
        var apiKey = "7eez6hvgkcgs5y3fc4px4mmb"; 
        var query = "http://api.hotwire.com/v1/search/car?apikey=" +apiKey+ "&dest=" +pickUp+ "&startdate=" +startDate+ "&enddate=" +endDate+ "&pickuptime=" +startTime+ "&dropofftime=" +endTime+ "&format=jsonp";
        $.ajax({
            type: 'GET',
            url: query,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            success: function(data,status,jqxhr){
                $("#result").show();
                 $("#carResult").show();
                var carMetaData = jqxhr.responseJSON.MetaData.CarMetaData.CarTypes;
                /*Fro Car Details*/
                $.each(carMetaData, function(index){
                    $("#result").append('<div class="row">'+
                            '<div class="col-xs-6 col-sm-6">'+
                            '<img src="Resources/images/Standard_SUV_correct.png" class="img-responsive" alt="car"/>'+
                            '<div class="car-type">'+carMetaData[index].CarTypeCode+'</div>'+
                            '<div class="car-name">'+carMetaData[index].CarTypeName+'</div>'+
                            '<div class="car-name">'+carMetaData[index].TypicalSeating+'</div>'+
                            '</div>'+
                            '</div>'
                        );
                });
                /* For currncy details */
                    var Result = jqxhr.responseJSON.Result;
                    $.each(Result, function(index){
                      console.log(Result);
                      $("#carResult").append(
                            '<div class="row">'+
                            '<div class="col-xs-6 col-sm-6">'+
                            '<div class="car-type price">'+'<p>'+'<b>'+Result[index].CurrencyCode+ +Result[index].DailyRate+ '</b>'+'<p>'+'</div>'+
                            '<div class="car-name">'+'<p>'+'<b>'+ 'Total Price'+ ''  +Result[index].TotalPrice+ 'FOR'+ ' ' +Result[index].RentalDays+ 'DAY'+'</b>'+'</p>'+'</div>'+
                            '<a href='+Result[index].DeepLink+'>'+ 'Continue'+ '</a>'+
                            '</div>'+
                            '</div>'
                            );
                  });
            },
            error: function() { console.log('Not Success'); }
            
        });
        }
        

    /*Autocomplete*/
$("#form-pick-up").autocomplete({
                source: function( request, response ) {
                $.ajax({
                    url: "code.json",
                    dataType: "json",
                    data: {term: request.term},
                    success: function(data) {
                                response($.map(data, function(item) {
                                return {
                                    label: item.abbrev
                                   };
                            }));
                        }
                    });
                },
                minLength: 2
            });

});
